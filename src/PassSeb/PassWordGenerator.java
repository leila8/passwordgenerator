package PassSeb;

/**********
**By Seb**
***********/	

import java.util.Random;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import java.awt.Dimension;



public class PassWordGenerator {

	public static void main(String[] args) {
		
		
		        int length = 12; // Longueur du mot de passe

		        JFrame frame = new JFrame("PassWordGenerator");
		        JLabel print = new JLabel();
		        
		        JLabel espace = new JLabel("\n   \n");
				frame.add(espace);
		        
		        print.setText(new String(generatePswd(length))); // char[] --> String pour le JLabel
		        print.setAlignmentX(0.5f);
		        print.setAlignmentY(0.5f);
		        print.setPreferredSize(new Dimension(300, 120));
		        
		        frame.setLocationRelativeTo(null);
		        frame.setLayout(new BoxLayout(frame.getContentPane(), BoxLayout.Y_AXIS));
		        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		        frame.add(print);
		        frame.pack();
		        frame.setVisible(true);
		        
		        JLabel espace1 = new JLabel("\n   \n");
				frame.add(espace1);	    
		        
				JButton boutonNewPswd = new JButton("New password");
				boutonNewPswd.setAlignmentX(0.5f);
				boutonNewPswd.addActionListener(event-> print.setText(new String(generatePswd(length))));
				frame.add(boutonNewPswd);
				
				JLabel espace2 = new JLabel("\n   \n");
				frame.add(espace2);
				
				JButton boutonQuitter = new JButton("Quitter");
				boutonQuitter.setAlignmentX(0.5f);
				boutonQuitter.addActionListener(e->frame.dispose());
				frame.add(boutonQuitter);
				
				
				JLabel espace3 = new JLabel("\n   \n");
				frame.add(espace3);
				
				frame.setIconImage(new ImageIcon(PassWordGenerator.class.getResource("cryptee-icone-9007-48.png")).getImage());			    
			
		        frame.setVisible(true);
		     
		    }
	
		
		  /*  static char[] generatePswd(int len)
		    {
		       	        
		        		        
		    }*/
		}
